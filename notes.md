# Talk preparation

1. Create some notes
2. Come up with a story
3. Work out a script and timings
4. Create slides
5. Rehearse
6. Revise
7. Rehearse again

## Story

6 types of stories for a talk:

1. Enlightenment
    - A journly from ignorance to knowledge
2. Solution quest
    - A problem and multiple attempted solutions to that problem
3. A to Z
    - Talk about things in order
    - for example chronology or, if talking about say HTTP, buttom up (tcp -> http -> html, or something)
4. Show & tell
    - Demo, then explain
5. Theme & variations
    - Multiple ways to solve one problem
6. The catalog
    - This section

### Example

| Section          | Story |
| ---              | ---   |
| Audience         | 5     |
| Talk preparation | 3     |
| - Timeslots      | 5     |
| - Stories        | 6     |
| - Making slides  | 3     |
| 7 habits         | 5     |

## Script

Be pessimistic about time

### Example

1. Funny entrance       5 min
2. TOC                  1 min
3. First section        5 min

# Notes

For each problem:

1. Explain why we needed to do things
2. Describe the problem we ran in to
3. Describe our solution
4. [Optional] Future work
5. [Optional] Better solutions

## Narrative

Perhaps a chronological narrative, how we began by choosing software followed by producing things followed by maintaining them?

## How not to give a talk

https://www.youtube.com/watch?v=iE9y3gyF8Kw&feature=youtu.be&t=1h35m22s

1. Do not hide behind the podium
    - Walk around
2. Do not talk about yourself or the company
    - Say who you are on the intro slide
        - "I'm Linus and I work for Manetos'
    - Only mention what is relevant to the talk
        - What we do is important to understand the talk
3. Do not read everything on every slide
    - Rehears
    - Speaker notes
        - Bullet points
        - Just reminders
        - (for sharing the slides?)
4. Do not cram too much into each slide
    - one idea = one slide
    - less is more
    - keep a margin for bad environments on slides (say for a misaligned projector)
5. Cover everything in the talk description
    - Ask jfocus to revise the description if that isn't possible
6. Keep track of time
    - Rehears
    - Use a timer
7. Don't panic
    - Work around/move on from the problem
    - Bad stuff happens
    - Don't panic!

## Topicality

Present:

- Stuff you know
- Are currently topical
- You're enthusiastic about
- You can cover in the time allotted

Ask yourself:

- What do you expect the audience to learn?
- What do you expect the audience to do after the talk?

## ???

- Questions during talk or at the end?
- How does the microphone work?
- Slide remote?
- How to get a hold of me? twitter?
- 7-10 hours of preparation once all the content is done

## TODO

- Record an entire session to review (for preparation)
- Test presentation without internet
- Create clear separation between sections in slides
- Consider who the audience is and what their experiences are
    - My guess is they are mostly backend developers, thus clarifying the problem-domain (embedded system) would be useful
- Write about the limitations of unattended-upgrades and why we wrote our own
- Talk about IDEs vs gcc?
