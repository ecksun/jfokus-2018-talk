# Short-list

- [.] Development tools
    - [.] IAR workbench
    - [ ] Other tooling
- [ ] Maintenance
    - [ ] Sensor stealing

# Expanded sections

### Development tools

Long ago we started out with an embedded system written in C and a backend
system written in Haskell. Our main haskell-guy then left and we were left with
a codebase we had a hard time maintaining. Thus we started looking at Scala, it
made the porting from haskell easier than most other languages and was still
language easier to grasp. We have since moved on to mainly Node.js and Python
for our stack as they are easy to work with and fits with our culture well.

As much as we can we try to make sure that all our tools and software are open
source and as close to upstream as possible. In general this works well as we
can't really afford to maintain forks of things and allows us a flexibility we
like.

There are however many areas where this is not possible, our first strugles
with this was our microcontroller. One issue with the tooling and IDE around
our microcontroller was that you really only could build it with the help of
the IDE, which only ran on windows, moreover the license for the IDE was rather
large, meaning we could really only afford one. This made it very difficult to
build a CI/CD pipeline so it ended up with the entire codebase only being
buildable on one machine, which obviously isn't maintainable in the long run.

Since then we are always tring to make sure all our tools are both open source,
can run on our machines (mainly linux machines) and can be easily automated. We
have also since then outsourced the production of that component so that is not
our concern anymore

## Future

- Next generation
    - Kernel version issues
    - Better sensors?
