---
title: About our talk at JFocus 2017

---

# From Beaglebone to Boiler room – kernel issues and electrical noise

Taking our product, a smart heating control system, from a prototype and
getting it into the hands of consumers has proved to be a non-trivial task.

We chose software stacks, development tools and communication protocols. We
figured out monitoring, maintenance and upgrades. We worked with our production
partners on provisioning, producing systems and CE certification. Hear the
benefits of and problems with the choices we made in our quest.
